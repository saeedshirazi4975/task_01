import datetime
import jwt
from random import choices

from django.conf import settings
from django.core.cache import cache

from .models import AreaCode



create_token = lambda user_id, user_agent: jwt.encode(
    payload = {
        "id": user_id,
        "user-agent": user_agent,
        "iot": int(datetime.datetime.utcnow().strftime(settings.STR_FORMAT)),
        "exp": int((datetime.datetime.utcnow() + datetime.timedelta(hours=24)).strftime(settings.STR_FORMAT))
    }, 
    key = settings.SECRET_KEY, 
    algorithm = "HS256"
)

def phone_number_is_valid(phone_number):
    if phone_number.__len__() != 11:
        return False
    if not phone_number.isdigit():
        return False
    return True

def send_otp_service(phone_number):
    try:
        area_code = AreaCode.objects.get(area_code=phone_number[:4])
        operator = area_code.operator
        sms_service = operator.sms_service
        otp_code = ''.join(choices(population=settings.DIGITS, k=6))
        
        cache.set("phone_number", phone_number, 240)
        cache.set("otp-code", otp_code, 240)

        with open(f"{settings.BASE_DIR}/sms-log.txt", "w") as file:
            file.write(f"{area_code.area_code}:{operator.title}:{sms_service.title}:OTPcode-{otp_code}")
        
        return True
    except Exception as ex:
        print(ex)
        return False