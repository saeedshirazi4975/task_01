from django.shortcuts import get_object_or_404
from django.contrib.auth.hashers import make_password
from django.conf import settings
from django.core.cache import cache

from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_409_CONFLICT, HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST, HTTP_406_NOT_ACCEPTABLE
from rest_framework.permissions import IsAuthenticated

from .models import User, Token
from .serializers import UserSerializer, TokenSerializer
from .services import create_token, phone_number_is_valid, send_otp_service
from .authentication import CustomAuthentication


@api_view(["GET"])
def index(request):
    data = {
        "register": settings.BASE_URL+'api/register/',
        "login": settings.BASE_URL+'api/login/',
        "logout": settings.BASE_URL+'api/logout/',
        "change-pass": settings.BASE_URL+'api/change-pass/',
        "forgot-pass": settings.BASE_URL+'api/forgot-pass/',
        "list-tokens": settings.BASE_URL+'api/list-tokens/',
        "kill-token": settings.BASE_URL+'api/kill-token/',
        "send-otp": settings.BASE_URL+'api/send-otp/',
        "validate-otp": settings.BASE_URL+'api/validate-otp/',
    }
    return Response(data=data, status=HTTP_200_OK)


@api_view(['POST'])
def register(request):
    request.data["password"] = make_password(request.data["password"])
    serilizer = UserSerializer(data=request.data)
    if serilizer.is_valid():
        serilizer.save()
        return Response(data=serilizer.data, status=HTTP_201_CREATED)
    else:
        return Response(status=HTTP_409_CONFLICT)


@api_view(['POST'])
def login(request):
    if not request.META["HTTP_USER_AGENT"]:
        return Response(status=HTTP_400_BAD_REQUEST)
    username = request.data['username']
    password = request.data['password']
    user_agent = request.META["HTTP_USER_AGENT"]
    user = User.objects.get(username=username)
    if user is None:
        return Response(status=HTTP_404_NOT_FOUND)
    if not user.check_password(password):
        return Response(status=HTTP_400_BAD_REQUEST)
    token = create_token(user.id, user_agent)
    token_instance = Token(user=user, token=token, user_agent=user_agent)
    token_instance.save()
    resp = Response()
    resp.set_cookie(key="jwt", value=token, httponly=True)
    return resp


@api_view(['GET'])
@authentication_classes([CustomAuthentication])
@permission_classes([IsAuthenticated])
def list_tokens(request):
    tokens = request.user.tokens.all()
    serializer = TokenSerializer(tokens, many=True)
    return Response(data=serializer.data, status=HTTP_200_OK)


@api_view(['DELETE'])
@authentication_classes([CustomAuthentication])
@permission_classes([IsAuthenticated])
def kill_token(request):
    was_in = False
    curr_token = Token.objects.get(token=request.COOKIES.get("jwt"))
    token_ids = request.data["token-ids"]
    if curr_token.id in token_ids:
        token_ids.remove(curr_token.id)
        was_in = True
    for id in token_ids:
        get_object_or_404(Token, pk=id).delete()

    if was_in:
        return Response(data={f"token-{curr_token.id}": "Not deleted because it is your current token."}, status=HTTP_200_OK)
    return Response(data={f"success": "All tokens are deleted."}, status=HTTP_200_OK)


@api_view(['GET'])
@authentication_classes([CustomAuthentication])
@permission_classes([IsAuthenticated])
def logout(request):
    get_object_or_404(Token, token=request.COOKIES.get("jwt")).delete()
    resp = Response()
    resp.delete_cookie("jwt")
    resp.data = {"logout": "Successfully!"}
    resp.status = HTTP_200_OK
    return resp
    

@api_view(["PUT"])
@authentication_classes([CustomAuthentication])
@permission_classes([IsAuthenticated])
def change_pass(request):
    user = request.user
    curr_pass = request.data["current-password"]
    new_pass = request.data["new-password"]
    new_pass_rep = request.data["new-password-repeat"]
    
    if not user.check_password(curr_pass):
        return Response(data={"error": "Incorrect password"}, status=HTTP_406_NOT_ACCEPTABLE)
    
    if new_pass != new_pass_rep:
        return Response(data={"error": "new-password and new-password-repeat must be same"}, status=HTTP_406_NOT_ACCEPTABLE)

    user.set_password(new_pass)
    user.save()
    return Response(data={"change-password": "Successfully"}, status=HTTP_200_OK)


@api_view(["POST"])
def send_otp(request):
    phone_number = request.data["phone-number"]
    
    if not phone_number_is_valid(phone_number):
        return Response(data={"error": "Phone number is not valid."}, status=HTTP_406_NOT_ACCEPTABLE)
    
    if not send_otp_service(phone_number):
        return Response(data={"error": "Phone number is incorrect."}, status=HTTP_406_NOT_ACCEPTABLE)

    return Response(data={"success": "the otp code in sent."}, status=HTTP_200_OK)

@api_view(["POST"])
def validate_otp(request):
    _otp_code = cache.get("otp-code")
    print(_otp_code)
    otp_code = request.data["otp-code"]

    if _otp_code != otp_code:
        return Response(data={"error": "OTP code in incorrect."}, status=HTTP_406_NOT_ACCEPTABLE)
    
    return Response(status=HTTP_200_OK)


@api_view(["POST"])
def forgot_pass(request):
    _otp_code = cache.get("otp-code")
    otp_code = request.data["otp-code"]
    new_pass = request.data["new-password"]
    new_pass_rep = request.data["new-password-repeat"]

    if _otp_code != otp_code:
        return Response(data={"error": "OTP code in incorrect."}, status=HTTP_406_NOT_ACCEPTABLE)
    
    if new_pass != new_pass_rep:
        return Response(data={"error": "new-password and new-password-repeat must be same"}, status=HTTP_406_NOT_ACCEPTABLE)

    user = User.objects.get(phone_number=cache.get("phone_number"))
    user.set_password(new_pass)
    user.save()

    cache.delete_many(["phone_number", "otp-code"])

    return Response(status=HTTP_200_OK)
