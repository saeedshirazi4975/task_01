from rest_framework.authentication import BaseAuthentication
import jwt
from django.conf import settings
from rest_framework import exceptions

from .models import User, Token

class CustomAuthentication(BaseAuthentication):
    
    def authenticate(self, request):
        _token = request.COOKIES.get("jwt")

        if not _token:
            raise exceptions.NotAuthenticated()

        try:
            user = Token.objects.get(token=_token).user
        except Exception as ex:
            raise exceptions.NotAuthenticated()

        return (user, None)