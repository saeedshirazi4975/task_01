from rest_framework.serializers import ModelSerializer

from .models import User, Token

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class TokenSerializer(ModelSerializer):
    class Meta:
        model = Token
        fields = '__all__'
