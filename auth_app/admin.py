from django.contrib import admin

from .models import User, Token, Operator, AreaCode, SmsService

admin.site.register(User)
admin.site.register(Token)
admin.site.register(Operator)
admin.site.register(AreaCode)
admin.site.register(SmsService)