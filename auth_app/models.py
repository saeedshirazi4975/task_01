from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

class UserManager(BaseUserManager):
    def create_user(self, username, password, phone_number):
        
        if not username:
            raise ValueError('Users must have a username.')

        if not password:
            raise ValueError('Users must have a password.')

        if not phone_number:
            raise ValueError('Users must have a phone_number.')

        user = self.model(
            username = username,
            phone_number = phone_number,
        )
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password, phone_number):
        user = self.create_user(
            username = username, 
            password = password, 
            phone_number = phone_number
        )
        user.is_admin = True
        user.save()
        return user

class User(AbstractBaseUser):
    def phone_validator(value):
        if value.__len__() != 11:
            raise ValueError('Phone number must be 11 characters.')
        if not value.isdigit():
            raise ValueError('Phone number must be digit.')

    username = models.CharField(max_length=40, unique=True)
    phone_number = models.CharField(unique=True, max_length=11, validators=[phone_validator])
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['phone_number']

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

class Token(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name="tokens", blank=False, null=False)
    token = models.CharField(max_length=255, blank=False, null=False)
    user_agent = models.CharField(max_length=255, blank=False, null=False)
    
    def __str__(self):
        return self.token

class Operator(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    off_percent = models.IntegerField()

    def __str__(self):
        return self.title

class AreaCode(models.Model):
    def area_code_validator(value):
        if value.__len__() != 4:
            raise ValueError('Phone number must be 4 characters.')
        if not value.isdigit():
            raise ValueError('Phone number must be digit.')
        
    area_code = models.CharField(max_length=4, validators=[area_code_validator], null=False, blank=False)
    operator = models.ForeignKey(to=Operator, on_delete=models.CASCADE, related_name="area_codes") 

    def __str__(self):
        return self.area_code

class SmsService(models.Model):
    title = models.CharField(max_length=64, null=False, blank=False)
    operator = models.OneToOneField(to=Operator, on_delete=models.CASCADE, related_name="sms_service")
    api = models.URLField(null=False, blank=False)

    def __str__(self):
        return self.title