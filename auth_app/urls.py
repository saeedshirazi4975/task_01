from unicodedata import name
from django.urls import path

from .views import index, register, login, list_tokens, kill_token, logout, change_pass, send_otp, validate_otp, forgot_pass

urlpatterns = [
    path('', index, name='menu'),
    
    path('register/', register, name='register'),
    path('login/', login, name='login'),
    path("logout/", logout, name="logout"),
    
    path("change-pass/", change_pass, name="change-pass"),
    path("forgot-pass/", forgot_pass, name="forgot-pass"),

    path("list-tokens/", list_tokens, name="list-tokens"),
    path("kill-token/", kill_token, name="kill_token"),

    path("send-otp/", send_otp, name="send-otp"),
    path("validate-otp/", validate_otp, name="validate-otp"),

]
